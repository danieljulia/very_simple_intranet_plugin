# very_simple_intranet_plugin

A plugin which creates a very simple intranet based on a custom post type which is only visible for registered users, plus a new role that is able to create this type.
You must have the registration for users enabled in Wordpress options.

# Instructions

Features:

- Creates a new rol "intranet_user" who can create "intranet_page" contents
- Anonymous users are unable to see this post type. Logged users can.
- When trying to see a "intranet_page" anonymous users are redirected to login page.


# To-do list 

- Widgets?
- Send suggestions to dani@pimpampum.net or at twitter https://twitter.com/daniel_julia