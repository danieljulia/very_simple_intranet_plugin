<?php
/*
Plugin Name: Very simple intranet
Description: Creates a post type only visible for registered users
Version: 0.2
Author: Pimpampum
Author URI: http://www.pimpampum.net
License: GPL2
*/


// add a role for intranet users

function add_intranet_management_role() {
    add_role('intranet_user',
               'Intranet user',
               array(
                   'read' => true,
                   'edit_posts' => false,
                   'delete_posts' => false,
                   'publish_posts' => false,
                   'upload_files' => true,
               )
           );
}
add_action( 'init', 'add_intranet_management_role' );

// Register Custom Post Type "Intranet"
function custom_post_type() {

	$labels = array(
		'name'                  => _x( 'Intranet pages', 'Post Type General Name', 'ppp' ),
		'singular_name'         => _x( 'Intranet page', 'Post Type Singular Name', 'ppp' ),
		'menu_name'             => __( 'Intranet', 'ppp' ),
        'name_admin_bar'        => __( 'Intranet', 'ppp' ),
	);
	$args = array(
		'label'                 => __( 'Intranet page', 'ppp' ),
		'description'           => __( 'A post type only for registered users', 'ppp' ),
		'labels'                => $labels,
		'supports'              => array( 'author','title', 'editor','thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'menu_icon'             => 'dashicons-buddicons-buddypress-logo',
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
        'publicly_queryable'    => true,

        'capabilities' => array(
            'edit_post' => 'edit_intranet_page',
            'read_post' => 'read_intranet_page',
            'delete_post' => 'delete_intranet_page',
            'edit_posts' => 'edit_intranet_pages',
            //'edit_others_posts' => 'edit_others_intranet_pages',
            'publish_posts' => 'publish_intranet_pages',
            'read_private_posts' => 'read_private_intranet_pages',
            'edit_private_posts'=>'edit_private_intranet_pages',
            'edit_published_posts'=>'edit_published_intranet_pages',
            'edit_page' => 'edit_intranet_page',
            'create_posts' => 'edit_intranet_pages'
        ),
        'map_meta_cap'        => true,
        'capability_type'       => 'page',
        'show_in_rest'          => true

	);
	register_post_type( 'intranet_page', $args );

}
add_action( 'init', 'custom_post_type', 0 );


//fix a bug that displays the edit post link?

/*
add_action( 'admin_init', 'admin_remove_menu_pages' );

function admin_remove_menu_pages() {
    remove_menu_page( 'edit.php' );
}
*/

//if you are not logged you are unable to see this post types

add_action("template_redirect","prevent_accessing_intranet_posts_types");
function prevent_accessing_intranet_posts_types(){
     if( ! is_user_logged_in() && in_array( get_post_type(), array( 'intranet_page' ) ) ){
           wp_redirect( wp_login_url() );
           exit();
     }
}




//intranet users are able to create and edit intranet contents

add_action('admin_init','intranet_add_role_caps',999);

function intranet_add_role_caps() {


    $roles = array('intranet_user','editor','administrator');

    // Loop through each role and assign capabilities
    foreach($roles as $the_role) {

        $role = get_role($the_role);

        $role->add_cap( 'read' );
        $role->add_cap( 'read_intranet_page');
        $role->add_cap( 'delete_intranet_page');
        $role->add_cap( 'read_private_intranet_pages' );
        $role->add_cap( 'edit_intranet_page' );
        $role->add_cap( 'edit_intranet_pages' );
        $role->add_cap( 'edit_private_intranet_pages' );

        //$role->add_cap( 'edit_others_intranet_pages' );
        //$role->add_cap( 'edit_published_intranet_pages' );
        $role->add_cap( 'publish_intranet_pages' );
       // $role->add_cap( 'delete_others_intranet_pages' );
        $role->add_cap( 'delete_private_intranet_pages' );
       // $role->add_cap( 'delete_published_intranet_pages' );



    }

}

//remove admin color scheme options

function admin_color_scheme() {
    global $_wp_admin_css_colors;
    $_wp_admin_css_colors = 0;
 }
 add_action('admin_head', 'admin_color_scheme');
